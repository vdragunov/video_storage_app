@echo off
echo Wellcome to Video Storage Client install process!
echo.

echo Step ^[1^] Python 2.7.8 ^(32 bit^) will be installed. Its directory is on: %HOMEDRIVE%\Python27

set errors=0

REM ver | find "XP" > nul
REM if %ERRORLEVEL% EQU 0 ( goto win_5 ) else ( goto win_6 )
REM :win_5
REM 	echo Windows NT ^=^= 5.1 detected
REM 	msiexec ALLUSERS="1" TARGETDIR="%HOMEDRIVE%\Python27" /qn /i %CD%\python-2.7.6.msi
REM 	if %ERRORLEVEL% EQU 1 set /a errors+=1
REM 	echo Python 2.7.6 has been installed.
REM 	echo Apllying Python XP patch...
REM 	ren %HOMEDRIVE%\Python27\Lib\mimetypes.py mimetypes.py.bak
REM 	copy %CD%\xp_buggy\mimetypes.py %HOMEDRIVE%\Python27\Lib > nul
REM 	echo Python has been patched.
REM 	goto pygtk
REM :win_6
REM 	echo Windows NT ^> 5.1 detected
REM 	msiexec ALLUSERS="1" TARGETDIR="%HOMEDRIVE%\Python27" /qn /i %CD%\python-2.7.6.msi
REM 	if %ERRORLEVEL% EQU 1 set /a errors+=1
REM 	echo Python 2.7.6 has been installed.
REM 	goto pygtk
REM :pygtk
REM echo.

msiexec ALLUSERS="1" TARGETDIR="%HOMEDRIVE%\Python27" /qn /i %CD%\python-2.7.8.msi
if %ERRORLEVEL% EQU 1 set /a errors+=1
echo Python 2.7.8 ^(32 bit^) has been installed.
echo.

echo Step ^[2^] PyGTK 2.24 ^(32 bit^) will be installed. Its directory is on: %HOMEDRIVE%\Python27\lib\site-packages
msiexec TARGETDIR="%HOMEDRIVE%\Python27" /qn /i %CD%\pygtk-all-in-one-2.24.2.win32-py2.7.msi
if %ERRORLEVEL% EQU 1 set /a errors+=1

REM echo ^[3^] PIP 1.5.4 is installing... Its directory is on: %HOMEDRIVE%\Python27\lib\site-packages
REM %HOMEDRIVE%\Python27\python.exe get-pip.py
REM if %ERRORLEVEL% EQU 1 set /a errors+=1
REM echo.

echo Step ^[3^] The latest version of setuptools package will be installed. Its directory is on: %HOMEDRIVE%\Python27\lib\site-packages
%HOMEDRIVE%\Python27\python.exe %CD%\ez_setup.py
if %ERRORLEVEL% EQU 1 set /a errors+=1
echo.

echo Step ^[4^] Google OAuth 2.0 Python Client Library installation process...
REM %HOMEDRIVE%\Python27\Scripts\pip.exe install --upgrade google-api-python-client
cd %CD%\google-api-python-client-1.2
%HOMEDRIVE%\Python27\python.exe setup.py install
cd ..
if %ERRORLEVEL% EQU 1 set /a errors+=1
echo.

echo Installation has been finished, with %errors% errors.
pause

