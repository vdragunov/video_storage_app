# -*- coding: utf-8 -*-
import gtk
import gobject
from gtk import gdk
from datetime import datetime
from time import sleep
import os, re, urllib2, json, webbrowser, pickle, threading
import xml.etree.ElementTree as ET
import httplib
import httplib2
from apiclient.discovery import build
from apiclient.errors import HttpError
from apiclient.http import MediaFileUpload
import framework

gobject.threads_init()

RETRIABLE_EXCEPTIONS = (httplib2.HttpLib2Error, IOError, httplib.NotConnected,
  httplib.IncompleteRead, httplib.ImproperConnectionState,
  httplib.CannotSendRequest, httplib.CannotSendHeader,
  httplib.ResponseNotReady, httplib.BadStatusLine)

httplib2.RETRIES = 1

MAX_RETRIES = 10

RETRIABLE_STATUS_CODES = [500, 502, 503, 504]

class selectedVideoFile( gtk.HBox ):
    def __init__( self, absfilepath, db_id = 0 ):
        super(gtk.HBox, self).__init__()
        self.filepath = os.path.dirname( absfilepath )
        self.filename = os.path.basename( absfilepath )
        
        self.db_id = db_id
        self.match_exten = re.compile( r'^.+(\.[0-9a-zA-Z]+){1}$' ) #[0-9a-zA-Z\u0400-\u04ff_@]
        #self.match_title = re.compile(r'(^[0-9a-zA-Z_@\.]+)(\.?[0-9a-zA-Z]+)?$')
        
        self.extension = self.match_exten.search( self.filename ).group(1) if self.match_exten.search( self.filename ) else ''
        #self.title = self.match_title.search( self.filename ).group(1)
        
        self.label_filename = gtk.Label()
        self.label_filename.set_markup( "<span font_desc='11.5' weight='bold'>" + self.filename + "</span>" )
        
        self.spinner = gtk.Spinner()
        self.spinner.set_size_request(17,17)
        
        self.pack_start( self.label_filename, expand = False, padding = 5)
        self.pack_start( self.spinner, expand = False, padding = 5)
        
        self.spinner.set_no_show_all(True)
    
    def getFilePath(self):
        return self.filepath
    
    def setFileTitle(self,  name ):
        os.rename(  os.path.join( self.filepath, self.filename ), os.path.join( self.filepath, name ) )
        self.filename = name
        
    
    def setId(self,  new_id ):
        self.db_id = long(new_id)
        
    def getFileExt(self):
        return self.extension
        
    def getFileTitle(self):
        return self.filename
    
    def getId(self):
        return self.db_id
        
    def setColor(self, name ):
        self.label_filename.set_markup( "<span font_desc='11.5' foreground='" + name + "' weight='bold'>" + self.filename + "</span>" )

class appNotebook( gtk.Notebook ):
    def __init__(self, builder=None, main_app = None ):
        super(gtk.Notebook, self).__init__()
        self.set_show_tabs(False)
        self.set_show_border(False)
        
        self.selected_video_files = []
        #self.base_url = base_url
        self.builder = builder
        self.main_app = main_app
        
        self.login_page_box = builder.get_object( 'login_box' )
        self.main_page_box = builder.get_object( 'main_box' )
        # self.process_page_box = builder.get_object( 'process_box' )
        # self.video_chooser_dialog = builder.get_object( 'video_chooser_dialog' )

        self.backupServiceTreeView = builder.get_object('BackupServiceTreeView')
        self.streamerTreeView = builder.get_object('StreamerTreeView')
        self.filedeviceTreeView = builder.get_object('FileDeviceTreeView')

        self.main_popup_menu = builder.get_object('menu_RestoreTreeViews')
        self.main_popup_menuitems = [
            builder.get_object('restore_selected_item')
        ]

        self.backupServiceTreeView.get_selection().set_mode( gtk.SELECTION_MULTIPLE )
        self.streamerTreeView.get_selection().set_mode( gtk.SELECTION_MULTIPLE )
        self.filedeviceTreeView.get_selection().set_mode( gtk.SELECTION_MULTIPLE )


        self.BS_model = self.backupServiceTreeView.get_model()
        self.S_model = self.streamerTreeView.get_model()
        self.FD_model = self.filedeviceTreeView.get_model()

        self.object_models = [ self.FD_model, self.S_model, self.BS_model ]



        
        
        
        
        
        # self.process_page_account_butt = builder.get_object( 'account_link_butt' )
        # self.process_page_account_name = builder.get_object( 'account_uname' )
        # self.process_page_file_choose_butt = builder.get_object( 'file_choose_butt' )
        # self.process_page_clear_butt = builder.get_object( 'clear_butt' )
        # self.process_page_execute_butt = builder.get_object( 'execute_butt' )
        # self.selected_files_box = builder.get_object( 'selected_files' )
        
        # self.select_file_butt = builder.get_object( 'select_file_butt' )
        # self.cancel_file_butt = builder.get_object( 'cancel_file_butt' )
        
       
        # self.process_page_execute_butt.set_sensitive(False)
        # self.process_page_clear_butt.set_sensitive(False)
        self.main_app.main_toolbar_logout_button.set_sensitive(False)
        self.main_app.main_toolbar_refresh_button.set_sensitive(False)
        self.main_app.main_toolbar_backup_button.set_sensitive(False)
        self.main_app.main_toolbar_restore_button.set_sensitive(False)
        self.main_app.main_toolbar_ident_button.set_sensitive(False)
        self.main_app.main_toolbar_exit_button.set_sensitive(False)
        self.main_app.main_toolbar_clear_selected_button.set_sensitive(False)
        self.main_app.main_toolbar_add_device_button.set_sensitive(False)
        
        
        self.append_page( self.login_page_box )
        self.append_page( self.main_page_box )
        # self.append_page( self.process_page_box )
        
        self.backupServiceTreeView.connect( 'button-release-event', self.show_main_popup_menu )
        self.filedeviceTreeView.connect( 'button-release-event', self.show_main_popup_menu )
        self.streamerTreeView.connect( 'button-release-event', self.show_main_popup_menu )

        # self.cancel_file_butt.connect( "clicked", lambda button: self.video_chooser_dialog.hide() )
        # self.select_file_butt.connect( "clicked", self._addVideoFiles )
        # self.process_page_file_choose_butt.connect( "clicked", lambda button: self.video_chooser_dialog.run() )
        # self.process_page_clear_butt.connect( "clicked", self._clearSelectedFiles )
        # self.process_page_execute_butt.connect( "clicked", self._processAndSendFiles )
        # for widget in self.main_popup_menuitems: widget.show()
        # for widget in self.add_entry_popup_menuitems: widget.show()
        self.main_popup_menu.show_all()


    def show_main_popup_menu(self, widget, event):
        if event.button == 3:
            self.main_popup_menu.popup( None, None, None, event.button, event.time )
    
    def clear_main_tree_view(self):
        for model in self.object_models: model.clear()

    def refresh_main_tree_view( self ):

        self.clear_main_tree_view()

        for i in xrange( len( self.object_models ) ):
            root_storage_type = self.main_app.user_xml_db.find( framework.STORAGE_TYPES[ i ] )
            for storage in root_storage_type:
                storage_iter = self.object_models[ i ].append( None, [ framework.GTK_STOCK_ICONS[ i ], storage.find('storage_identifier').text, None, storage.find('owner').get('email'), storage.find('name').text, None, None, storage.find('add_date').text, None ] )

                for project in storage.find('projects'):
                    project_iter = self.object_models[ i ].append( storage_iter, [ framework.GTK_STOCK_ICONS[ -2 ], project.get('id'), None, storage.find('owner').get('email'), project.find('title').text, project.find('restore_path').text, None, project.find('add_date').text, project.find('modify_date').text ] )

                    for entry in project.find('entries'):
                        self.object_models[ i ].append( project_iter, [ framework.GTK_STOCK_ICONS[-1], entry.get('id'), None, entry.find('owner').get('email'), entry.find('title').text, entry.find('restore_path').text, entry.find('size').text, entry.find('add_date').text, entry.find('modify_date').text ] )                        

                for entry in storage.find('entries'):
                    self.object_models[ i ].append( storage_iter, [ framework.GTK_STOCK_ICONS[-1], entry.get('id'), None, entry.find('owner').get('email'), entry.find('title').text, entry.find('restore_path').text, entry.find('size').text, entry.find('add_date').text, entry.find('modify_date').text ] )

    def _getAuthorizationAPI( self ):
        http = httplib2.Http()
        if self.user_credential.access_token_expired:
            self.user_credential.refresh(http)
        return build( 'youtube', 'v3', http = self.user_credential.authorize( http ) )
        
    def _initializeUpload( self ):
        amt_of_files = len( self.selected_video_files )
        reserved_data = {
            'app_token' : framework.APP_TOKEN,
            'user_id' : self.user_id,
            'video_amt' : amt_of_files,
            'video_titles' : [video.getFileTitle() for video in self.selected_video_files]
        }
        request = urllib2.Request( self.base_url + '/appreserve/', json.dumps( reserved_data ), {'Content-Type' : 'application/json'} )
        response = urllib2.urlopen( request )
        received_data = json.loads( response.read() )
        for i in xrange( amt_of_files ):
            self.selected_video_files[i].setId( received_data['video_ids'][i] )
            new_file_title_id =  str( self.selected_video_files[i].getId() ) + self.selected_video_files[i].getFileExt()
            self.selected_video_files[i].setFileTitle( new_file_title_id )
            
        return amt_of_files
            
    def _resumableUpload( self, request, video=0 ):
        response = None
        error = None
        retry = 0
        while response is None:
            try:
                print "Uploading file..."
                
                status, response = request.next_chunk()
                if 'id' in response:
                    print "'%s' (video id: %s) was successfully uploaded." % ( self.selected_video_files[video].getFileTitle() , response['id'] )
                else:
                    return ( False,  "The upload failed with an unexpected response: %s" % response )
            except HttpError, e:
                if e.resp.status in RETRIABLE_STATUS_CODES:
                    error = "A retriable HTTP error %d occurred:\n%s" % (e.resp.status, e.content)
                else:
                    raise
            except RETRIABLE_EXCEPTIONS, e:
                error = "A retriable error occurred: %s" % e

            if error is not None:
                print error
                
                retry += 1
                if retry > MAX_RETRIES:
                    return ( False ,"No longer attempting to retry." )

                max_sleep = 2 ** retry
                sleep_seconds = random.random() * max_sleep
                print "Sleeping %f seconds and then retrying..." % sleep_seconds
                sleep(sleep_seconds)
        return ( response['id'], "Okay!, Retries: %d" % retry )

        
    
        #~ self.next_page()
        #connect to db, check username and pass and get User.id
        
        
    def _addVideoFiles( self, button ):
        
        filenames = self.video_chooser_dialog.get_filenames() 
        if filenames:
            self.process_page_execute_butt.set_sensitive(True)
            self.process_page_clear_butt.set_sensitive(True)
            for filename in filenames:
                
                selected_file = selectedVideoFile( filename )
                self.selected_video_files.append( selected_file )
                self.selected_files_box.pack_start( selected_file, expand = False )
                
            self.selected_files_box.show_all()
            self.video_chooser_dialog.unselect_all()
        
        self.video_chooser_dialog.hide()
    
    def _processAndSendFiles(self, button):
        self.process_page_execute_butt.set_sensitive(False)
        
        upload_manager = threading.Thread( target = self._startUploadManagerThread )
        upload_manager.start()
    
    def _clearSelectedFiles(self, button):
        for old_selected_file in self.selected_video_files:
            self.selected_files_box.remove( old_selected_file )
        self.process_page_clear_butt.set_sensitive(False)
    
    def _startUploadManagerThread( self ):
        
        youtube = self._getAuthorizationAPI()
        amt = self._initializeUpload()
        youtube_uis = []
        
        
        for video in xrange( amt ):
        
            tags = None
            request = youtube.videos().insert( part="snippet,status",
                body = dict(
                snippet = dict(
                title = self.selected_video_files[video].getFileTitle(),
                description = 'Video Storage data ui: %d' % self.selected_video_files[video].getId(),
                tags = tags,
                categoryId = 1
                ),
                status = dict( privacyStatus='unlisted' )
                ),
                media_body = MediaFileUpload( os.path.join( self.selected_video_files[video].getFilePath(), self.selected_video_files[video].getFileTitle() ) , chunksize=-1, resumable=True)
            )
            t = threading.Thread( target = self._startVideoUploadThread, args = (youtube_uis, request, video) )
            t.start()
            t.join()
        
        uploaded_data = {
            'app_token' : framework.APP_TOKEN,
            'video_ids' : [ video_id.getId() for video_id in self.selected_video_files ],
            'video_raw_titles' : [ video_title.getFileTitle() for video_title in self.selected_video_files ],
            'youtube_uis' : youtube_uis,
            'video_amt' : amt,
        }
        
        vs_request = urllib2.Request( self.base_url + '/appadddata/', json.dumps( uploaded_data ), {'Content-Type' : 'application/json'} )
        response = urllib2.urlopen( vs_request )
        
        gobject.idle_add( self.process_page_execute_butt.set_sensitive, True )
        
    def _startVideoUploadThread( self, youtube_uis, request, video ):
        
        def set_video_label_status( colour ):
            self.selected_video_files[video].setColor( colour )
            return False
            
        def set_spinner_activity( flg ):
            if flg:
                self.selected_video_files[video].spinner.show()
                self.selected_video_files[video].spinner.start()
            else:
                self.selected_video_files[video].spinner.stop()
                self.selected_video_files[video].spinner.hide()
                
            return False
        
        
        gobject.idle_add( set_video_label_status, 'orange' )
        gobject.idle_add( set_spinner_activity, True )
        
        response = self._resumableUpload( request, video )
        
        youtube_uis.append( response[0] )
        
        if not response[0]: gobject.idle_add( set_video_label_status, 'red' )
        else: gobject.idle_add( set_video_label_status, 'green' )
        gobject.idle_add( set_spinner_activity, False )



class MainApp():
    def __init__(self, glade_file = None, base_url = 'http://127.0.0.1:8000'):

        self.user_config_path = None
        self.user_login = None
        self.user_pass = None
        self.user_remember = 0
        self.user_id = 0
        self.user_credential = None
        self.base_url = base_url
        self.login_states = [False, False]
        self.account_link = 'http://localhost/'
        self.user_xml_db = None
        self.user_paths = {
            'users_dir' : os.path.join( os.getcwdu(), 'user' ),
            'login_config' : os.path.join( os.getcwdu(), 'user', 'last_login_config.xml' )
        }
        self.os_platform = os.name

        self.builder = gtk.Builder()
        self.builder.add_from_file( glade_file )

        self.main_toolbar = self.builder.get_object( 'main_toolbar' )
        self.main_toolbar_logout_button = self.builder.get_object('logout_button')
        self.main_toolbar_refresh_button = self.builder.get_object('refresh_button')
        self.main_toolbar_backup_button = self.builder.get_object('backup_button')
        self.main_toolbar_restore_button = self.builder.get_object('restore_button')
        self.main_toolbar_clear_selected_button = self.builder.get_object('clear_selected_button')
        self.main_toolbar_add_device_button = self.builder.get_object('add_device_button')
        self.main_toolbar_ident_button = self.builder.get_object('ident_button')
        self.main_toolbar_exit_button = self.builder.get_object('exit_button')

        self.login_butt = self.builder.get_object( 'login_butt' )
        self.logout_butt = self.builder.get_object( 'logout_butt' )
        self.auth_error_alert = self.builder.get_object( 'auth_err' )
        self.login_page_uname = self.builder.get_object( 'uname_field' )
        self.login_page_pass = self.builder.get_object( 'pass_field' )
        self.login_page_remember_me = self.builder.get_object( 'remember_me_butt' )

        self._check_user_config()

        self.main_box = gtk.VBox()

        self.main_window = gtk.Window()
        self.main_window.set_size_request( 480, -1 )
        self.main_window.set_resizable(False)
        self.main_window.set_title("Video Storage App v.2.1")
        self.main_window.set_position( gtk.WIN_POS_CENTER )

        self.main_waiter = framework.Waiter( self.builder )
        self.main_notebook = appNotebook( self.builder, self )
        self.backup_dialog = framework.BackupDialog( self.builder, self )
        self.add_device_dialog = framework.DeviceAddDialog( self.builder, self )

        self.backup_dialog.set_transient_for( self.main_window )
        self.backup_dialog.set_position( gtk.WIN_POS_CENTER_ON_PARENT )

        #self.login_page_remember_me.set_sensitive(False)
        #self.login_butt.set_sensitive( self.user_remember )
        self.auth_error_alert.set_no_show_all(True)
        self.auth_error_alert.hide()

        self.main_window.connect("destroy", gtk.main_quit)
        self.main_toolbar_logout_button.connect( 'clicked', self.logoutFromVideostorage )
        self.login_page_uname.connect("key-release-event", self._entryLoginHandler, 0)
        self.login_page_pass.connect("key-release-event", self._entryLoginHandler, 1)
        self.login_butt.connect( "clicked", self.loginToVideostorage )
        self.logout_butt.connect( "clicked", self.logoutFromVideostorage )
        self.login_page_remember_me.connect( 'toggled', self._entryLoginRememberHandler )

        self.main_toolbar_backup_button.connect('clicked', self.show_backup_dialog )
        self.main_toolbar_add_device_button.connect('clicked', self.show_add_device_dialog )
        self.main_toolbar_refresh_button.connect('clicked', self.sync_archive_entries_handler )

        self.main_box.pack_start( self.main_toolbar, expand = False )
        self.main_box.pack_start( self.main_notebook )
        self.main_window.add( self.main_box )
        self.main_window.show_all()

    def show_add_device_dialog( self, button ):
        self.add_device_dialog.show_all()

    def show_backup_dialog( self, button ):
        self.backup_dialog.set_user(self.user_id)
        self.backup_dialog.show_all()

    def sync_archive_entries_handler( self, button ):
        syncing = threading.Thread( target = self.sync_archive_entries )
        syncing.start()

    def sync_archive_entries(self):

        self.main_waiter.set_parent( self.main_window )
        gobject.idle_add( self.main_waiter.start_waiter )

        self.load_user_archive_db()
        self.main_notebook.refresh_main_tree_view()

        gobject.idle_add( self.main_waiter.stop_waiter )

    def logoutFromVideostorage( self, button ):
        
        if not self.user_remember:
            self.login_page_uname.set_text('')
            self.login_page_pass.set_text('')
        
        # if self.selected_video_files:
        #     for old_selected_file in self.selected_video_files:
        #         self.selected_files_box.remove( old_selected_file )
        
            self.user_credential = None
            self.user_login = None
            self.user_pass = None
            self.account_link = 'http://localhost/'
            self.user_id = 0
            self.login_butt.set_sensitive(False)
            self.login_states = [False, False]

        self.main_notebook.clear_main_tree_view()

        self.main_toolbar_refresh_button.set_sensitive(False)
        self.main_toolbar_backup_button.set_sensitive(False)
        self.main_toolbar_add_device_button.set_sensitive(False)
        button.set_sensitive(False)
        self.main_window.set_resizable(False)
        self.main_window.set_default_size( -1, -1 )
        self.main_notebook.prev_page()
        print 'debug: successfully logout'

    def loginToVideostorage( self, button ):
        
        login_data = {
            'app_token' : framework.APP_TOKEN,
            'user_id' : self.user_id,
            'uname' : self.user_login if self.user_remember else self.login_page_uname.get_text(),
            'pass' : self.user_pass if self.user_remember else self.login_page_pass.get_text(),
            'hashed' : True if self.user_remember else False,
            'remember_me' : self.login_page_remember_me.get_active() if not self.user_remember else False
        }

        request = urllib2.Request( self.base_url + '/appgetuser/', json.dumps( login_data ), {'Content-Type' : 'application/json'} )
        try:
            response = urllib2.urlopen( request )
        except urllib2.URLError:
            self.auth_error_alert.show()
            return
        received_data = json.loads( response.read() )
        
        print received_data['status']
        
        if received_data['status'] == 'ok':
            
            self.user_id = received_data['user_id']
            self.user_login = received_data['user_login']
            self.account_link = received_data['user_link']
            self.user_paths['archive'] = os.path.join( os.getcwdu(), 'user', self.user_login + '_archive_db.xml' )

            #self.process_page_account_name.set_markup( "<span font_desc='11.5' underline='single' foreground='blue'>" + self.user_login + "</span>" )
            #self.process_page_account_butt.connect("clicked", lambda button, link: webbrowser.open(link), self.account_link)
            #self.user_credential = pickle.loads( received_data['pickled_credential'] )
            
            self.auth_error_alert.hide()
            
            print 'debug: successfully login'

            if received_data.get( 'hash', '' ) :
                
                if not os.path.lexists( self.user_paths['login_config'] ):
                    config_root = ET.Element( 'user', attrib = { 'id' : str( self.user_id ) } )
                    ET.SubElement( config_root, 'username' ).text = self.user_login
                    ET.SubElement( config_root, 'hash' ).text = received_data['hash']
                    ET.SubElement( config_root, 'remember_me', attrib = { 'status' : str(1) } )
                    ET.ElementTree( config_root ).write( self.user_paths['login_config'], encoding='UTF-8' )
                else:
                    try:
                        user_xml = ET.parse( self.user_paths['login_config'] )
                        user_xml_root = user_xml.getroot()
                        user_xml_root.find('user').items['id'] = str( self.user_id )
                        user_xml_root.find('username').text = self.user_login
                        user_xml_root.find('hash').text = received_data['hash']
                        user_xml_root.find('remember_me').attrib['status'] = str(1)
                        ET.ElementTree( user_xml_root ).write( self.user_paths['login_config'], encoding='UTF-8' )
                    except (IOError, OSError):
                        print 'info: can`t open file=`last_login_config.xml`'
                    except ET.ParseError:
                        print 'info: file=`last_login_config.xml`, is not a XML file'
                self.login_page_uname.set_sensitive(False)
                self.login_page_pass.set_sensitive(False)

            try:
                xml = ET.parse( self.user_paths['archive'] )
                self.user_xml_db = xml.getroot()
            except ( OSError, IOError, ET.ParseError ):
                self.load_user_archive_db()
            
            self.main_toolbar_refresh_button.set_sensitive(True)
            self.main_toolbar_backup_button.set_sensitive(True)
            self.main_toolbar_add_device_button.set_sensitive(True)
            self.main_toolbar_logout_button.set_sensitive(True)
            self.main_window.set_resizable(True)
            self.main_window.resize( 640, 640 )
            self.main_notebook.refresh_main_tree_view()
            self.main_notebook.next_page()


        else:
            self.auth_error_alert.show()

    def load_user_archive_db( self ):
        archive_path = os.path.join( os.getcwdu(), 'user', self.user_login + '_archive_db.xml' )
        login_data = {
            'app_token' : framework.APP_TOKEN,
            'user_id' : self.user_id
        }

        request = urllib2.Request( self.base_url + '/app_get_user_acc_archive/', json.dumps( login_data ), {'Content-Type' : 'application/json'} )
        try:
            response = urllib2.urlopen( request )
            received_data = json.loads( response.read() )
        except urllib2.URLError as e:
            print 'error: can`t fetch archive DB, because -> `{0}`'.format( e.reason )
        except urllib2.HTTPError as e:
            print 'error: can`t fetch archive DB, because HTTP response is {0}'.format( e.code )
        else:
            self.archive_to_xml( received_data, archive_path )
            print 'info: <user id={0}> archive DB fetched, succesfully'.format( self.user_id )

    def _check_user_config( self ):

        if not os.path.lexists( self.user_paths['users_dir'] ):
            try:
                os.mkdir( self.user_paths['users_dir'] )
            except ( OSError, IOError ):
                return False

        try:
            user_xml = ET.parse( self.user_paths[ 'login_config' ] )
            user_xml_root = user_xml.getroot()
            self.user_remember = int( user_xml_root.find('remember_me').attrib['status'] )
            if self.user_remember:
                self.user_id = user_xml_root.attrib['id']
                self.user_login = user_xml_root.find('username').text
                self.user_pass = user_xml_root.find('hash').text

                self.login_page_uname.set_text( self.user_login )
                self.login_page_pass.set_text( '*' * 16 )
                self.login_page_uname.set_sensitive(False)
                self.login_page_pass.set_sensitive(False)

                self.login_butt.set_sensitive(True)
                self.login_page_remember_me.set_active(True)
                return True
        except ( OSError, IOError, IndexError, ET.ParseError ): pass
        return False

    def _entryLoginRememberHandler( self, toggle_button ):

        if not toggle_button.get_active() and self.user_remember:

            self.login_page_uname.set_sensitive(True)
            self.login_page_uname.set_text('')
            self.login_page_pass.set_sensitive(True)
            self.login_page_pass.set_text('')

            try:
                os.remove( self.user_paths[ 'login_config' ] )
                self.user_remember = 0
            except OSError:
                print 'error: config file=`{0}`, doesn`t exist'.format( self.user_login + '_config.xml' )
            except IOError:
                print 'error: can`t find or open config file=`{0}`'.format( self.user_login + '_config.xml' )
            except ET.ParseError:
                print 'error: file=`{0}`, is not a XML file'.format( self.user_login + '_config.xml' )


    def _entryLoginHandler( self, entry, event, num ):
        if entry.get_text(): self.login_states[num] = True
        else: self.login_states[num] = False
        if all( self.login_states ) : self.login_butt.set_sensitive( True )
        else: self.login_butt.set_sensitive( False )
        return False
    @staticmethod
    def convert_epoch_time( sec = 0, json_t = True ):

        if json_t: return datetime.fromtimestamp( sec ).strftime( '%Y-%m-%dT%H:%M:%SZ' )
        else: return datetime.fromtimestamp( sec ).strftime( '%H:%M:%S %d.%m.%Y' )

    # def load_from_xml( self, path ):
    #     try:
    #         xml_path = os.path.join( os.getcwdu(), 'user', self.user_login + '_archive.xml' )
    #         user_xml = ET.parse( xml_path )
    #     except ( OSError, IOError, IndexError, ET.ParseError ): return None

    #     user_xml_root = user_xml.getroot()
    #     if user_xml_root.find( 'user' ).attrib['id'] == self.user_id:
    #         archive = dict()
    #         for storage_type in user_xml_root:
    #             archive[storage_type] = 

    #     else:
    #         return None




    def archive_to_xml( self, data, save_path, root_name = 'archive' ):

        self.user_xml_db = ET.Element( root_name )
        ET.SubElement( self.user_xml_db, 'user', attrib = {'id' : str( data['user_id'] ), 'email' : data['user_email']} )
        for storage_type in framework.STORAGE_TYPES: 
            storage_type_root = ET.SubElement( self.user_xml_db, storage_type )

            for storage in data[ storage_type ]:
                #storage_root = ET.SubElement( storage_type_root, 'storage', attrib = { 'id' : storage['id'], 'storage_type' : storage['storage_type'] } )
                storage_root = ET.SubElement( storage_type_root, 'storage', attrib = { 'id' : str( storage['id'] ) } )
                ET.SubElement( storage_root, 'storage_identifier' ).text = storage['storage_identifier']
                ET.SubElement( storage_root, 'name' ).text = storage['name']
                ET.SubElement( storage_root, 'owner', attrib = { 'id' : str( storage['owner__id'] ), 'email' : storage['owner__email'] } )
                ET.SubElement( storage_root, 'add_date' ).text = storage['add_date']
                ET.SubElement( storage_root, 'description' ).text = storage['description']
                ET.SubElement( storage_root, 'all_space' ).text = str( storage['all_space'] )
                ET.SubElement( storage_root, 'free_space' ).text = str( storage['free_space'] )
                ET.SubElement( storage_root, 'state' ).text = storage['state']

                projects_root = ET.SubElement( storage_root, 'projects' )
                for project in storage['projects']:
                    project_root = ET.SubElement( projects_root, 'project', attrib = { 'id' : str( project['id'] ) } )
                    ET.SubElement( project_root, 'title' ).text = project['title']
                    ET.SubElement( project_root, 'owner', attrib = { 'id' : str( project['owner__id'] ), 'email' : project['owner__email'] } )
                    ET.SubElement( project_root, 'restore_path' ).text = project['restore_path']
                    ET.SubElement( project_root, 'youtube_ui' ).text = project['youtube_ui']
                    ET.SubElement( project_root, 'net_link' ).text = project['net_link']
                    ET.SubElement( project_root, 'add_date' ).text = project['add_date']
                    ET.SubElement( project_root, 'modify_date' ).text = project['modify_date']
                    ET.SubElement( project_root, 'description' ).text = project['description']
                    ET.SubElement( project_root, 'status' ).text = project['status']
                    
                    entries_root = ET.SubElement( project_root, 'entries' )
                    for entry in project['entries']:
                        entry_root = ET.SubElement( entries_root, 'entry', attrib = { 'id' : str( entry['id'] ), 'entry_type' : entry['entry_type'] } )
                        ET.SubElement( entry_root, 'title' ).text = entry['title']
                        ET.SubElement( entry_root, 'raw_title' ).text = entry['raw_title']
                        ET.SubElement( entry_root, 'owner', attrib = { 'id' : str( entry['owner__id'] ), 'email' : entry['owner__email'] } )
                        ET.SubElement( entry_root, 'restore_path' ).text = entry['restore_path']
                        ET.SubElement( entry_root, 'youtube_ui' ).text = entry['youtube_ui']
                        ET.SubElement( entry_root, 'add_date' ).text = entry['add_date']
                        ET.SubElement( entry_root, 'modify_date' ).text = entry['modify_date']
                        ET.SubElement( entry_root, 'size' ).text = str( entry['size'] )
                        ET.SubElement( entry_root, 'filemark' ).text = str( entry['filemark'] )
                        ET.SubElement( entry_root, 'net_link' ).text = entry['net_link']
                        ET.SubElement( entry_root, 'meta_data' ).text = entry['meta_data']
                        ET.SubElement( entry_root, 'hash_sha1' ).text = entry['hash_sha1']
                        ET.SubElement( entry_root, 'description' ).text = entry['description']

                entries_root = ET.SubElement( storage_root, 'entries' )
                for entry in storage['entries']:
                    entry_root = ET.SubElement( entries_root, 'entry', attrib = { 'id' : str( entry['id'] ), 'entry_type' : entry['entry_type'] } )
                    ET.SubElement( entry_root, 'title' ).text = entry['title']
                    ET.SubElement( entry_root, 'raw_title' ).text = entry['raw_title']
                    ET.SubElement( entry_root, 'owner', attrib = { 'id' : str( entry['owner__id'] ), 'email' : entry['owner__email'] } )
                    ET.SubElement( entry_root, 'restore_path' ).text = entry['restore_path']
                    ET.SubElement( entry_root, 'youtube_ui' ).text = entry['youtube_ui']
                    ET.SubElement( entry_root, 'add_date' ).text = entry['add_date']
                    ET.SubElement( entry_root, 'modify_date' ).text = entry['modify_date']
                    ET.SubElement( entry_root, 'size' ).text = str( entry['size'] )
                    ET.SubElement( entry_root, 'filemark' ).text = str( entry['filemark'] )
                    ET.SubElement( entry_root, 'net_link' ).text = entry['net_link']
                    ET.SubElement( entry_root, 'meta_data' ).text = entry['meta_data']
                    ET.SubElement( entry_root, 'hash_sha1' ).text = entry['hash_sha1']
                    ET.SubElement( entry_root, 'description' ).text = entry['description']
        ET.ElementTree( self.user_xml_db ).write( save_path, encoding = 'UTF-8' )
        return 0


main_app = MainApp( 'ui_struct.glade' )
gtk.main()
        
# builder = gtk.Builder()
# builder.add_from_file('ui_struct.glade')
# mainWindow = gtk.Window()
# mainWindow.set_size_request( 480, -1 )
# mainWindow.set_resizable(False)
# mainWindow.set_title("Video Storage App v.1")
# mainWindow.connect("destroy", gtk.main_quit)

# notebook = appNotebook( builder )

# mainWindow.add( notebook )
# mainWindow.show_all()
# gtk.main()
#~ auth_err = builder.get_object("auth_err_align")
#~ auth_err.hide()
