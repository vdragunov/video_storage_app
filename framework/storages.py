# -*- coding: utf-8 -*-
from utils import *

class DeviceAddDialog( gtk.Window, OperationManager ):
    def __init__(self, builder, main_app):
        super(gtk.Window, self).__init__()
        OperationManager.__init__( self, main_app.os_platform )

        self.builder = builder
        self.main_app = main_app
        self.set_transient_for( main_app.main_window )
        self.set_position( gtk.WIN_POS_CENTER_ON_PARENT )
        self.set_modal(True)
        self.set_title('Добавить новый носитель')

        self.selected_storage_type = None
        self.device_creators = {
            'DD' : self.create_disk_device,
            'CD' : self.create_cartridge_device
        }
        self.device_locators = {
            'DD' : self.get_disks,
            'CD' : self.get_streamers
        }

        self.box = self.builder.get_object('main_add_device_box_dialog')
        self.add( self.box )

        self.cancel_button = builder.get_object('cancel_add_device')
        self.add_button = builder.get_object('add_add_device')
        self.locate_button = builder.get_object('locate_device_button')
        self.radio_dd_button = builder.get_object('dd_radio')
        self.radio_cd_button = builder.get_object('cd_radio')
        self.radio_default = builder.get_object('default_radio')
        self.device_combo = builder.get_object('device_combo')

        self.common_chooser_dialog = builder.get_object('commonChooserDialog')
        self.common_chooser_dialog_cancel = builder.get_object('cancel_file_butt')
        self.common_chooser_dialog_select = builder.get_object('select_file_butt')

        self.radio_cd_button.set_active(False)
        self.radio_dd_button.set_active(False)
        self.radio_default.set_active(True)

        self.locate_button.set_sensitive(False)
        self.add_button.set_sensitive(False)
        # self.common_chooser_dialog.set_select_multiple(False)
        self.device_combo.set_sensitive(False)
        self.radio_default.set_no_show_all(True)

        # self.common_chooser_dialog_cancel.connect( 'clicked', lambda button: self.common_chooser_dialog.hide() )
        # self.common_chooser_dialog_select.connect( 'clicked', self.get_device_path )

        self.connect( 'delete-event', lambda widget, event: self.hide_self(True) )
        self.cancel_button.connect( 'clicked', lambda widget: self.hide_self(True) )

        self.add_button.connect( 'clicked', self.add_button_handler )
        self.locate_button.connect( 'clicked', self.locate_button_handler )
        self.device_combo.connect( 'changed', self.check_combo_select )
        self.radio_cd_button.connect( 'toggled', self.toggle_type_device, 'CD' )
        self.radio_dd_button.connect( 'toggled', self.toggle_type_device, 'DD' )

    def check_combo_select(self, combo_box):
        if self.device_combo.get_active_iter(): self.add_button.set_sensitive(True)
        else: self.add_button.set_sensitive(False)

    def toggle_type_device( self, radio, select_type ):
        for i in xrange(3):
            if select_type == STORAGE_TYPES[i]:

                self.selected_storage_type = STORAGE_TYPES[i]
                self.locate_button.set_sensitive(True)
                return None

    def create_disk_device(self, device):

        new_disk_meta = None

        for ( num, disk ) in self.located_devices.iteritems():
            if device == num:
                new_disk_meta = disk

        if not new_disk_meta:
            print 'error: Can`t find selected disk'
            return
        device_data = {
            'app_token' : APP_TOKEN,
            'user_id' : self.main_app.user_id,
            'type' : 'storage',
            'name' : new_disk_meta['model'],
            'storage_identifier' : new_disk_meta['SN'],
            'all_space' : new_disk_meta['size'],
            'free_space' : new_disk_meta['freespace'],
            'description' : None,
            'state' : STORAGE_STATES[2],
            'storage_type' : self.selected_storage_type
            }

        request = urllib2.Request( self.main_app.base_url + '/app_add_user_object/', json.dumps( device_data ), {'Content-Type' : 'application/json'} )
        try:
            response = urllib2.urlopen( request )
            received_data = json.loads( response.read() )

            disk_id = received_data['object_id']

            added_disk = DiskDevice( disk_id, self.main_app.user_id, new_disk_meta['SN'], new_disk_meta['letters'][0] )

            print received_data['messages']

        except urllib2.URLError:
            print 'error: Service is unavailavle, <disk name=`{0}`> is not added, try later.'.format( new_disk_meta['model'] )

    def create_cartridge_device(self, device):
        pass

    def create_new_storage(self, device):
        self.main_app.main_waiter.set_parent(self)
        gobject.idle_add( self.main_app.main_waiter.start_waiter )


        self.device_creators[ self.selected_storage_type ]( device )
        gobject.idle_add( self.hide_self, False )

        gobject.idle_add( self.main_app.main_waiter.stop_waiter )

    def device_combo_append(self, device):
        self.device_combo.get_model().append( [ device['num'], device['model'] + '_{0}'.format(device['num']) ] )
        return False

    def exec_device_locator( self ):
        
        self.main_app.main_waiter.set_parent(self)
        gobject.idle_add( self.main_app.main_waiter.start_waiter )

        self.device_locators[ self.selected_storage_type ]()
        for ( num, device ) in self.located_devices.iteritems():

            gobject.idle_add( self.device_combo_append, device )

        self.device_combo.set_sensitive(True)

        gobject.idle_add( self.main_app.main_waiter.stop_waiter )


    def locate_button_handler( self, button ):

        self.device_combo.get_model().clear()

        locating = threading.Thread( target = self.exec_device_locator )
        locating.start()

    def add_button_handler( self, button ):
        active_device_num = self.device_combo.get_model().get_value( self.device_combo.get_active_iter(), 0 )
        adding = threading.Thread( target = self.create_new_storage, args = (active_device_num,) )
        adding.start()

    def hide_self(self, ret):
        self.hide()
        self.device_combo.get_model().clear()
        self.locate_button.set_sensitive(False)
        self.add_button.set_sensitive(False)
        self.device_combo.set_sensitive(False)
        self.radio_default.set_active(True)
        return ret

class DiskDevice():

    def __init__( self, archive_id, owner_id, storage_id, mount_path, name = None, all_space = None, free_space = None, owner_email = None, model = None ):
        self.gtk_stock_icon = gtk.STOCK_HARDDISK
        self.id = archive_id
        self.name = name
        self.disk_sn = storage_id
        self.disk_model = model
        self.all_space = all_space
        self.free_space = free_space
        self.owner_id = owner_id
        self.owner_email = owner_email

        self.mount_path = mount_path
        self.base_dir_path = os.path.join( self.mount_path, os.sep, 'disk_id=' + str( self.id ) + '_sn=' + str(self.disk_sn) )
        self.state = STORAGE_STATES[2]
        self.type = STORAGE_TYPES[0]
        self.creation_date = datetime.today()
        self.session = None
        self.disk_paths = {
            'base_dir_path' : self.base_dir_path,
            'index_path' : os.path.join( self.base_dir_path, str(self.owner_id) + '_archive_db.xml' ),
            'content_dir_path' : os.path.join( self.base_dir_path, 'content' )
        }
        
        self.check_disk_struct()

        try:
            et_tree = ET.parse( self.disk_paths['index_path'] )
            self.xml_index_db = xml.getroot()
        except ( ET.ParseError, OSError, IOError ):
            self.create_new_xml_db()
            #self.xml_index_db = None

    def create_new_xml_db(self):
        self.xml_index_db = ET.Element( 'storage', attib = { 'id' : str(self.id) } )
        ET.SubElement( self.xml_index_db, 'owner', attib = { 'id' : str(self.owner_id), 'email' : self.owner_email } )
        ET.SubElement( self.xml_index_db, 'storage_identifier' ).text = self.disk_sn
        ET.SubElement( self.xml_index_db, 'name' ).text = self.name
        ET.SubElement( self.xml_index_db, 'add_date' ).text = self.creation_date.strftime( '%Y-%m-%dT%H:%M:%SZ' )
        ET.SubElement( self.xml_index_db, 'all_space' ).text = str(self.all_space)
        ET.SubElement( self.xml_index_db, 'free_space' ).text = str(self.free_space)
        ET.SubElement( self.xml_index_db, 'state' ).text = self.state
        ET.SubElement( self.xml_index_db, 'type' ).text = self.type
        ET.SubElement( self.xml_index_db, 'projects' )
        ET.SubElement( self.xml_index_db, 'entries' )
        ET.ElementTree( self.xml_index_db ).write( self.disk_paths['index_path'], encoding = 'UTF-8' )
    # запускать в отдельном потоке
    def add_session( self, session ):
        if self.session:
            self.session = session
        else:
            print 'error: Empty session can not be stored'

    def session_upload( self, upload_link ):
        
        for ( session_id, entry ) in self.session.iteritems():
            session_data = {
                'app_token' : APP_TOKEN,
                'user_id' : self.owner_id,
                'type' : 'entry'
            }

            if not entry.is_entry():
                session_data['add_type'] = 'project'
                session_data['title'] = entry.filename
                session_data['restore_path'] = entry.path
                session_data['description'] = 'None'

                request = urllib2.Request( upload_link, json.dumps( session_data ), {'Content-Type' : 'application/json'} )
                try:
                    response = urllib2.urlopen( request )
                except urllib2.URLError:
                    print 'error: Can`t upload meta of <project title=`{0}`>'.format( entry.title )
                    continue
                received_data = json.loads( response.read() )
                entry.set_id( received_data['object_id'] )
                entry.set_status( received_data['status'] )
                print received_data['messages']

                session_data = {
                    'app_token' : APP_TOKEN,
                    'user_id' : self.owner_id,
                    'type' : 'entry',
                    'add_type' : 'entry'
                }
                
                for ( project_session_id, project_entry ) in entry.iteritems():
                    session_data['title'] = project_entry.filename
                    session_data['restore_path'] = project_entry.path
                    session_data['description'] = None
                    session_data['size'] = project_entry.size
                    session_data['hash_sha1'] = None
                    session_data['meta_data'] = None
                    session_data['filemark'] = None
                    session_data['entry_type'] = ENTRY_TYPES[0]
                    session_data['connected_project_id'] = entry.id

                    request = urllib2.Request( upload_link, json.dumps( session_data ), {'Content-Type' : 'application/json'} )
                    try:
                        response = urllib2.urlopen( request )
                        received_data = json.loads( response.read() )
                        project_entry.set_id( received_data['object_id'] )
                        project_entry.set_status( received_data['status'] )
                        print received_data['messages']
                    except urllib2.URLError:
                        print 'error: Can`t upload meta of <project id=`{0}`> <entry title=`{1}`>'.format( entry.id, project_entry.title )
                        continue

            else:
                session_data['add_type'] = 'entry'
                session_data['title'] = entry.filename
                session_data['restore_path'] = entry.path
                session_data['description'] = None
                session_data['size'] = entry.size
                session_data['hash_sha1'] = None
                session_data['meta_data'] = None
                session_data['filemark'] = None
                session_data['entry_type'] = ENTRY_TYPES[0]

                request = urllib2.Request( upload_link, json.dumps( session_data ), {'Content-Type' : 'application/json'} )
                try:
                    response = urllib2.urlopen( request )
                    received_data = json.loads( response.read() )
                    entry.set_id( received_data['object_id'] )
                    entry.set_status( received_data['status'] )
                    print received_data['messages']

                except urllib2.URLError:
                    print 'error: Can`t upload meta of <entry title=`{0}`>'.format( entry.title )
                    continue
            
    def create_session_xml_slice( self ):
        xml_projects_root = self.xml_index_db.find( 'projects' )
        xml_entries_root = self.xml_index_db.find( 'entries' )
        
        for ( session_id, entry ) in self.session.iteritems():
                
                if entry.get_status() == 'copied':

                    if not entry.is_entry():

                        current_project_root =  ET.SubElement( xml_projects_root, 'project', attib = { 'id' : str( entry.id ) } )
                        ET.SubElement( current_project_root, 'title' ).text = entry.filename
                        ET.SubElement( current_project_root, 'owner', attrib = { 'id' : str( entry.owner_id ), 'email' : str(None) } )
                        ET.SubElement( current_project_root, 'restore_path' ).text = entry.path
                        ET.SubElement( current_project_root, 'youtube_ui' )
                        ET.SubElement( current_project_root, 'net_link' )
                        ET.SubElement( current_project_root, 'add_date' ).text = entry.add_date
                        ET.SubElement( current_project_root, 'modify_date' )
                        ET.SubElement( current_project_root, 'description' )
                        ET.SubElement( current_project_root, 'status' ).text = entry.get_status()
                        current_project_entries_root = ET.SubElement( current_project_root, 'entries' )

                        for ( project_session_id, project_entry ) in entry.iteritems():
                            current_entry_root = ET.SubElement( current_project_entries_root, 'entry', attib = { 'id' : str( project_entry.id ) } )
                            ET.SubElement( current_entry_root, 'title' ).text = project_entry.filename
                            ET.SubElement( current_entry_root, 'raw_title' )
                            ET.SubElement( current_entry_root, 'owner', attrib = { 'id' : str( project_entry.owner_id ), 'email' : str(None) } )
                            ET.SubElement( current_entry_root, 'restore_path' ).text = project_entry.path
                            ET.SubElement( current_entry_root, 'youtube_ui' )
                            ET.SubElement( current_entry_root, 'add_date' ).text = project_entry.add_date
                            ET.SubElement( current_entry_root, 'modify_date' )
                            ET.SubElement( current_entry_root, 'size' ).text = str( project_entry.size )
                            ET.SubElement( current_entry_root, 'net_link' )
                            ET.SubElement( current_entry_root, 'meta_data' )
                            ET.SubElement( current_entry_root, 'hash_sha1' ).text = str( project_entry.hash_sha1 )
                            ET.SubElement( current_entry_root, 'description' )
                            ET.SubElement( current_entry_root, 'status' ).text = project_entry.get_status()
                    else:
                        current_entry_root = ET.SubElement( xml_entries_root, 'entry', attib = { 'id' : str( entry.id ) } )
                        ET.SubElement( current_entry_root, 'title' ).text = entry.filename
                        ET.SubElement( current_entry_root, 'raw_title' )
                        ET.SubElement( current_entry_root, 'owner', attrib = { 'id' : str( entry.owner_id ), 'email' : str(None) } )
                        ET.SubElement( current_entry_root, 'restore_path' ).text = entry.path
                        ET.SubElement( current_entry_root, 'youtube_ui' )
                        ET.SubElement( current_entry_root, 'add_date' ).text = entry.add_date
                        ET.SubElement( current_entry_root, 'modify_date' )
                        ET.SubElement( current_entry_root, 'size' ).text = str( entry.size )
                        ET.SubElement( current_entry_root, 'net_link' )
                        ET.SubElement( current_entry_root, 'meta_data' )
                        ET.SubElement( current_entry_root, 'hash_sha1' ).text = str( entry.hash_sha1 )
                        ET.SubElement( current_entry_root, 'description' )
                        ET.SubElement( current_entry_root, 'status' ).text = entry.get_status()
                else:
                    continue

        ET.ElementTree( self.xml_index_db ).write( self.disk_paths['index_path'], encoding = 'UTF-8' )

    def calc_hash( self, entry ):

        sha1 = hashlib.sha1()
        try:
            of = open( entry.get_file_path(), 'rb' )
            while True:
                chunk = of.read( 128 * sha1.block_size )
                if not chunk:
                    break
                sha1.update(chunk)
            entry.set_hash( sha1.hexdigest() )
            of.close()
        except (IOError, OSError):
            print 'error: Can`t open <entry | filename=`{0}`> with path=`{1}`'.format( entry.filename, entry.get_file_path() )

    def check_disk_struct(self):
        check_paths = [ self.disk_paths['base_dir_path'], self.disk_paths['content_dir_path'] ]
        for path in check_paths:
            if not os.path.lexists( path ):
                try:
                    os.mkdir( path )
                except OSError:
                    print 'error: Can`t create disk_dir at {0}'.format( path )
                    break


    def process_fs_objects( self, max_parallel_threads = 5 ):

        threads = []
        stdout_lock = threading.Lock()

        for ( session_id, entry ) in self.session.iteritems():
            copying = threading.Thread( target = self.copying_and_hashing, args = ( entry, stdout_lock ) )
            threads.append( copying )

        thread_slices = [ threads[ i : i + max_parallel_threads ] for i in xrange( 0, len( threads ), max_parallel_threads ) ]

        for thread_slice in thread_slices:
            for thread in thread_slice: thread.start()
            for thread in thread_slice: thread.join()

    def copying_and_hashing( self, entry, stdout_lock ):

        if not entry.is_entry():

            project_dir = os.path.join( self.disk_paths['content_dir_path'], 'project_' + str(entry.id) )
            try:
                os.mkdir( project_dir )
            except OSError:
                with stdout_lock:
                    print 'error: Can`t create dir for <project id={0}>'.format( entry.id )
                return

            for ( project_session_id, project_entry ) in entry.iteritems():
                self.calc_hash( project_entry )
                if project_entry.hash:
                    try:
                        shutil.copy( project_entry.get_file_path(), os.path.join( project_dir, 'entry_' + str(project_entry.id) ) )
                        project_entry.set_status( 'copied' )
                    except shutil.Error:
                        with stdout_lock:
                            print 'error: Can`t copy <entry path={0}> to `{1}`'.format( project_entry.get_file_path(), project_dir )
                        continue
            entry.set_status( 'copied' )
        else:    
            self.calc_hash( entry )
            if project_entry.hash:
                try:
                    shutil.copy( entry.get_file_path(), os.path.join( self.disk_paths['content_dir_path'], 'entry_' + str(entry.id) ) )
                    project_entry.set_status( 'copied' )
                except shutil.Error:
                    with stdout_lock:
                        print 'error: Can`t copy <entry path={0}> to `{1}`'.format( entry.get_file_path(), self.disk_paths['content_dir_path'] )

    def verify_session(self, verify_link):

        for ( session_id, entry ) in self.session.iteritems():
            session_data = {
                'app_token' : APP_TOKEN,
                'user_id' : self.owner_id
            }
            
            if not entry.is_entry():
                session_data['verify_type'] = 'project'
                session_data['object_id'] = entry.id
                session_data['status'] = 'added'


                request = urllib2.Request( verify_link, json.dumps( session_data ), {'Content-Type' : 'application/json'} )
                try:
                    response = urllib2.urlopen( request )
                except urllib2.URLError:
                    print 'error: Can`t verify meta of <project id={0}>'.format( entry.id )
                    continue
                received_data = json.loads( response.read() )
                entry.set_status( received_data['status'] )
                print received_data['messages']

                session_data = {
                    'app_token' : APP_TOKEN,
                    'user_id' : self.owner_id
                }

                for ( project_session_id, project_entry ) in entry.iteritems():
                    session_data['verify_type'] = 'entry'
                    session_data['object_id'] = project_entry.id
                    session_data['hash'] = str(project_entry.hash)
                    session_data['net_link'] = None
                    session_data['youtube_ui'] = None
                    session_data['status'] = 'added'

                    request = urllib2.Request( verify_link, json.dumps( session_data ), {'Content-Type' : 'application/json'} )
                    try:
                        response = urllib2.urlopen( request )
                        received_data = json.loads( response.read() )
                        project_entry.set_id( received_data['object_id'] )
                        project_entry.set_status( received_data['status'] )
                        print received_data['messages']
                    except urllib2.URLError:
                        print 'error: Can`t verify meta of <entry id=`{0}`>'.format( project_entry.id )
                        continue
            else:
                session_data['verify_type'] = 'entry'
                session_data['object_id'] = entry.id
                session_data['hash'] = str(entry.hash)
                session_data['net_link'] = None
                session_data['youtube_ui'] = None
                session_data['status'] = 'added'

                request = urllib2.Request( verify_link, json.dumps( session_data ), {'Content-Type' : 'application/json'} )
                try:
                    response = urllib2.urlopen( request )
                    received_data = json.loads( response.read() )
                    entry.set_id( received_data['object_id'] )
                    entry.set_status( received_data['status'] )
                    print received_data['messages']
                except urllib2.URLError:
                    print 'error: Can`t verify meta of <entry id=`{0}`>'.format( entry.id )
                    continue

        
    def set_id(self, db_id):
        self.id = db_id


class CartridgeDevice():
    def __init__( self, num, index = None ):
        self.gtk_stock_icon = gtk.STOCK_MEDIA_RECORD
        self.session_to_save = None
        self.xml_index_root = index
        self.tape_num = num
        self.index_shift = None
        #self.itdt_path = os.path.join( os.getcwdu(), 'ibm', 'itdt.exe' )
        # try:
        #   m = TAPE_MATCH.search( self.sys_path )
        #   self.win_tape_num = m.group( 1 )
        # except IndexError:
        #   self.win_tape_num = 0
        if os.name == 'posix':
            self.os_differs = { 'itdt_exec' : os.path.join( os.getcwdu(), 'ibm', 'itdt.sh' ), 'tape_path' : '/dev/IBMtape{0}'.format( self.tape_num ) }
        else:
            self.os_differs = { 'itdt_exec' : os.path.join( os.getcwdu(), 'ibm', 'itdt.exe' ), 'tape_path' : '\\\\.\\tape{0}'.format( self.tape_num ) }

    #запускать в новом потоке
    def restore_index( self ):
        args = [ self.os_differs['itdt_exec'], '-f', self.os_differs['tape_path'], '-w', '1' ]
        tmp_file = tempfile.NamedTemporaryFile( delete = False )
        tmp_file_path = tmp_file.name
        tmp_file.close()

        try:
            subprocess.check_output( args + ['list'] )
            itdt_list_resp = subprocess.check_output( args + ['list'] )
            m_code_list_obj = ITDT_EXIT_CODE.search( itdt_list_resp )

            if m_code_list_obj and ( m_code_list_obj.group(1) == '0' ):
                m_resp_obj = ITDT_LIST.findall( itdt_list_resp )
                if m_resp_obj:

                    try:
                        self.index_shift = int( m_resp_obj[-2] ) - 1
                        itdt_fsf_resp = subprocess.check_output( args + [ 'fsf', '%d' % self.index_shift ] )
                        m_code_fsf_resp = ITDT_EXIT_CODE.search( itdt_fsf_resp )

                        if m_code_fsf_resp and ( m_code_fsf_resp.group(1) == '0' ):

                            itdt_read_resp = subprocess.check_output( args + [ 'read', '-d', tmp_file_path ] )
                            m_code_read_resp = ITDT_EXIT_CODE.search( itdt_read_resp )

                            if m_code_read_resp and ( m_code_read_resp.group(1) == '0' ):
                                et_tree = ET.parse( tmp_file_path )
                                self.xml_index_root = et_tree.getroot()

                    except (IndexError, ValueError, ET.ParseError):
                        pass

        except subprocess.CalledProcessError:
            pass
        os.remove( tmp_file_path )

    def create_new_index( self ): pass

    def add_session( self, session, xml_slice ): pass

    def get_xml_index(self): return self.xml_index_root

    def set_xml_index(self, index): self.xml_index_root = index