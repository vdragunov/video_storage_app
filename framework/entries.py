# -*- coding: utf-8 -*-
from utils import *

class StorageProject():
    def __init__( self, name, path, size, add_date, owner = 0, entries = dict(), status = 'created' ):
        self.gtk_stock_icon = gtk.STOCK_DIRECTORY
        self.filename = name
        self.path = path
        self.size = size
        self.add_date = add_date
        self.status = status
        self.owner_id = owner
        self.entries = entries
        self.tree_row_ref = None
        self.id = None

    def iteritems(self):
        return self.entries.iteritems()

    def items(self):
        return self.entries.items()

    def __setitem__(self, key, value):
        self.entries[ key ] = value

    def __iter__(self):
        for elem in self.entries:
            yield elem

    def __getitem__(self, i):
        return self.entries[i]

    def __delitem__(self, key):
        del self.entries[ key ]

    def __len__(self):
        return len( self.entries )

    def get_file_path(self):
        return os.path.join( self.path, self.filename )

    def set_owner(self, owner):
        self.owner_id = owner

    def is_entry(self): return False

    def get_status(self): return self.status

    def set_status(self, status):
        self.status = status

    def set_tree_row_ref( self, tree_model, tree_path ):
        self.tree_row_ref = gtk.TreeRowReference( tree_model, tree_path )

    def __del__(self):
        self.entries.clear()

class StorageEntry():
    def __init__( self, name, path, size, add_date, owner = 0, status = 'created' ):
        self.gtk_stock_icon = gtk.STOCK_FILE
        self.filename = name
        self.path = path
        self.size = size
        self.add_date = add_date
        self.status = status
        self.hash = None
        self.owner_id = owner
        self.tree_row_ref = None
        self.id = None
        self.entry_type = 'OT'
        self.raw_title = None

    def get_file_path(self):
        return os.path.join( self.path, self.filename )

    def set_hash(self, sha_1):
        self.hash = sha_1

    def set_owner(self, owner):
        self.owner_id = owner

    def is_entry(self): return True

    def get_status(self): return self.status

    def set_status(self, status):
        self.status = status

    def set_tree_row_ref( self, tree_model, tree_path ):
        self.tree_row_ref = gtk.TreeRowReference( tree_model, tree_path )