# -*- coding: utf-8 -*-
from utils import *
from storages import DiskDevice, CartridgeDevice
from entries import StorageEntry, StorageProject

class BackupDialog( gtk.Window, OperationManager ):
    def __init__(self, builder, main_app):

        super(gtk.Window, self).__init__()
        OperationManager.__init__( self, main_app.os_platform )

        self.set_title('Добавление в архив')
        self.set_modal(True)
        self.set_size_request(640,480)

        self.main_app = main_app
        self.builder = builder
        self.user_id = main_app.user_id
        self.session = dict()
        self.acc_storages = {
            STORAGE_TYPES[0]: None,
            STORAGE_TYPES[1]: None,
            STORAGE_TYPES[2]: None
        }

        self.exec_session_ready = {
            'has_entries' : False,
            'has_places' : False
        }

        self.add_box = builder.get_object('main_add_box_dialog')
        self.addTreeView = builder.get_object('AddTreeView')
        self.accStorageTreeView = builder.get_object('AccStorageTreeView')
        self.common_chooser_dialog = builder.get_object('commonChooserDialog')

        self.add_cancel_button = builder.get_object('add_cancel_button')
        self.add_exec_button = builder.get_object('add_exec_button')
        self.add_toolbar_open = builder.get_object('add_toolbar_open')
        self.add_toolbar_refresh = builder.get_object('add_toolbar_refresh')
        self.add_toolbar_deselect = builder.get_object('add_toolbar_deselect')
        self.add_toolbar_selectall = builder.get_object('add_toolbar_selectall')
        self.common_chooser_dialog_cancel = builder.get_object('cancel_file_butt')
        self.common_chooser_dialog_select = builder.get_object('select_file_butt')
        self.overall_progressbar = builder.get_object('add_overall_progress')
        self.entry_progressbar = builder.get_object('add_current_progress')
        self.entry_view_message = builder.get_object('add_entry_message')
        self.cell_renderer_select = builder.get_object( 'cell_renderer_add' )

        self.add_entry_popup_menu = self.builder.get_object('menu_AddTreeView')
        self.add_entry_popup_menuitem_remove = self.builder.get_object('remove_selected_item')

        self.addTreeView.get_selection().set_mode( gtk.SELECTION_MULTIPLE )
        self.accStorageTreeView.get_selection().set_mode( gtk.SELECTION_MULTIPLE )
        self.ACC_model = self.accStorageTreeView.get_model()
        self.ADD_model = self.addTreeView.get_model()
        self.add( self.add_box )

        self.entry_view_message.set_text('Ожидание')
        self.add_exec_button.set_sensitive( False )

        self.connect( 'delete-event', lambda widget, event: self.hide_self() )
        self.add_cancel_button.connect( 'clicked', lambda widget: self.hide_self() )
        self.addTreeView.connect( 'button-release-event', self.show_add_entry_popup_menu )

        self.add_toolbar_open.connect( 'clicked', lambda widget: self.common_chooser_dialog.run() )
        self.add_toolbar_refresh.connect( 'clicked', self.add_toolbar_refresh_handler )
        self.add_toolbar_selectall.connect( 'clicked', self.add_toolbar_selectall_handler )
        self.add_toolbar_deselect.connect( 'clicked', self.add_toolbar_deselect_handler )
        self.common_chooser_dialog_cancel.connect( 'clicked', lambda button: self.common_chooser_dialog.hide() )
        self.common_chooser_dialog_select.connect( 'clicked', self.create_entry )
        self.add_entry_popup_menuitem_remove.connect( 'activate', self.delete_entry_from_session )
        self.cell_renderer_select.connect( 'toggled', self.toggle_acc_storages )
        self.connect( 'ready-to-exec', self.check_exec_ready_handler )
        

        # debug
        #self.add_exec_button.connect( 'clicked', self.get_selected_paths )
        self.add_exec_button.connect( 'clicked', self.add_exec_button_handler )

        self.add_entry_popup_menu.show_all()

    def add_exec_button_handler( self, button ):

        executing = threading.Thread( target = self.manage_added_entries_process )
        executing.start()

    def manage_added_entries_process(self):
        #temp only disks
        for mounted_device in self.acc_storages[ STORAGE_TYPES[0] ]:
            if mounted_device.get( 'mount', False ):

                disk_device = DiskDevice( mounted_device['id'], mounted_device['owner_id'], mounted_device['SN'], mounted_device['letters'][0] )
                disk_device.add_session( self.session )
                disk_device.session_upload( self.main_app.base_url + '/app_add_user_object/' )
                disk_device.process_fs_objects()
                disk_device.create_session_xml_slice()
                disk_device.verify_session()

        




    def check_exec_ready_handler(self, dialog):

        self.add_exec_button.set_sensitive( all( self.exec_session_ready.values() ) )

    def check_acc_storages(self):
        
        self.main_app.main_waiter.set_parent(self)
        gobject.idle_add( self.main_app.main_waiter.start_waiter )

        data = {
            'app_token' : APP_TOKEN,
            'user_id' : self.main_app.user_id
            }

        request = urllib2.Request( self.main_app.base_url + '/app_get_user_acc_places/', json.dumps( data ), {'Content-Type' : 'application/json'} )
        try:
            response = urllib2.urlopen( request )
            received_data = json.loads( response.read() )

            print received_data['messages']

        except urllib2.URLError:
            print 'error: Service is unavailavle, try later.'.format( new_disk_meta['model'] )
            return

        self.get_disks()
        self.acc_storages[ STORAGE_TYPES[0] ] = self.located_devices.values()
        print self.acc_storages[ STORAGE_TYPES[0] ]

        #--TODO--
        #self.get_streamers()
        #self.acc_storages[ STORAGE_TYPES[1] ] = self.located_devices.values()
        #--TODO--

        for s_type_num in xrange( len( STORAGE_TYPES ) ):

            for received_device in received_data[ STORAGE_TYPES[s_type_num] ]:

                device_iter = self.ACC_model.append( [ None, GTK_STOCK_ICONS[ s_type_num ], received_device['id'], received_device['name'], received_device['owner__email'], gtk.STOCK_NO, True ] ) # debug

                for located_device in self.acc_storages[ STORAGE_TYPES[s_type_num] ]:
                    if located_device['SN'] == received_device['storage_identifier']:

                        located_device['id'] = received_device['id']
                        located_device['mount'] = True
                        located_device['owner_id'] = received_device['owner__id']

                        self.ACC_model.set_value( device_iter, 5, gtk.STOCK_YES ) 
                        #self.ACC_model.set_value( device_iter, 6, True ) debug

        gobject.idle_add( self.main_app.main_waiter.stop_waiter )


    def toggle_acc_storages( self, cell, path ):
        self.ACC_model[ path ][ 0 ] = not self.ACC_model[ path ][ 0 ]
        self.exec_session_ready['has_places'] = False

        for device_row in self.ACC_model:
            if device_row[0]:
                self.exec_session_ready['has_places'] = True
                break
        self.emit('ready-to-exec')


    def add_toolbar_selectall_handler( self, button ):
        for i in xrange( len( self.ACC_model ) ):
            device_iter = self.ACC_model.get_iter( i )
            if self.ACC_model.get_value( device_iter, 6 ):
                self.ACC_model.set_value( device_iter, 0, True )

    def add_toolbar_deselect_handler( self, button ):

        for i in xrange( len( self.ACC_model ) ):
            device_iter = self.ACC_model.get_iter( i )
            self.ACC_model.set_value( device_iter, 0, False )

    def add_toolbar_refresh_handler( self, button ):

        self.ACC_model.clear()
        checking = threading.Thread( target = self.check_acc_storages )
        checking.start()

    def hide_self(self):
        self.hide()
        self.add_exec_button.set_sensitive( False )
        self.session.clear()
        self.ADD_model.clear()
        self.ACC_model.clear()
        return True

    def set_user(self, user_id):
        self.user_id = user_id

    def show_add_entry_popup_menu(self, widget, event):
            if event.button == 3:
                self.add_entry_popup_menu.popup( None, None, None, event.button, event.time )

    def create_entry( self, button ):
        abs_filenames = self.common_chooser_dialog.get_filenames()

        print 'selected files: {0}'.format(abs_filenames)

        if abs_filenames:
            local_format_datetime = datetime.today().strftime( '%Y-%m-%dT%H:%M:%SZ' )

            for abs_filename in abs_filenames:

                if os.path.isdir( abs_filename ):

                    local_project = StorageProject( os.path.basename( abs_filename ), os.path.dirname( abs_filename ), 0, local_format_datetime, self.user_id )

                    sub_dir_filenames = os.listdir( abs_filename )
                    print local_project.filename
                    print sub_dir_filenames # баг добавления всех файлов проектов ко всем проектам в сессию

                    for sub_dir_filename in sub_dir_filenames:
                        abs_path = os.path.join( abs_filename, sub_dir_filename )

                        print abs_path

                        if not os.path.isdir( abs_path ):

                            local_project_entry = StorageEntry( os.path.basename( abs_path ), os.path.dirname( abs_path ), os.path.getsize( abs_path ), local_format_datetime, self.user_id )

                            try:
                                last_id = max( local_project.entries )
                                local_project[ last_id + 1 ] = local_project_entry
                            except ValueError:
                                local_project[ 0 ] = local_project_entry
                            local_project.size += local_project_entry.size
                            del local_project_entry

                    if local_project.entries:
                        try:
                            last_id =  max( self.session )
                            self.session[ last_id + 1 ] = copy.deepcopy( local_project )
                        except ValueError:
                            self.session[ 0 ] = copy.deepcopy(local_project)

                    # local_project.entries.clear()
                    del local_project
                else:
                    local_entry = StorageEntry( os.path.basename( abs_filename ), os.path.dirname( abs_filename ), os.path.getsize( abs_filename ), local_format_datetime, self.user_id )
                    try:
                        last_id =  max( self.session )
                        self.session[ last_id + 1 ] = ( local_entry )
                    except ValueError:
                        self.session[ 0 ] = ( local_entry )
                    del local_entry

            self.refresh_add_tree_view()
        self.common_chooser_dialog.unselect_all()
        self.common_chooser_dialog.hide()
        

    def refresh_add_tree_view(self):
        self.exec_session_ready['has_entries'] = False

        if self.session:

            self.exec_session_ready['has_entries'] = True
            self.ADD_model.clear()

            for ( session_id, entry ) in self.session.iteritems():
                project_iter = self.ADD_model.append( None, [ entry.gtk_stock_icon, entry.filename, entry.path, entry.size, entry.add_date, entry.status, None, session_id ] )
                if not entry.is_entry():
                    for ( project_session_id, project_entry ) in entry.iteritems():
                        self.ADD_model.append( project_iter, [ project_entry.gtk_stock_icon, project_entry.filename, project_entry.path, project_entry.size, project_entry.add_date, project_entry.status, None, project_session_id ] )

        self.emit('ready-to-exec')                        
    
    def delete_entry_from_session( self, widget ):
        model, selected_entries_path_list = self.addTreeView.get_selection().get_selected_rows()
        if selected_entries_path_list:

            list_path_depth_0 = [ path for path in selected_entries_path_list if model.iter_depth( model.get_iter( path ) ) == 0 ]

            for entry_ref in [ gtk.TreeRowReference( model, path_0 ) for path_0 in list_path_depth_0 ]:
                entry_iter = model.get_iter( entry_ref.get_path() )
                session_id = model.get_value( entry_iter, 7 )
                del self.session[ session_id ]
                model.remove( entry_iter )

                # if model.iter_has_child( entry_iter ):
                #     session_id = model.get_value( entry_iter, 7 )
                #     while project_entry_first_child:
                #         model.remove( project_entry_first_child )
                #         project_entry_first_child = model.iter_children( entry_iter )
                #     del self.session[ session_id ]
                #     model.remove( entry_iter )        
                # else:


            #     entry_iter = model.get_iter( entry_ref.get_path() )
            #     # project_ref = 
            #     project_iter_parent = model.iter_parent( entry_iter )
            #     project_entry_first_child = model.iter_children( entry_iter )

            #     # удаляется ребенок из проекта
            #     if project_iter_parent:
            #         project_session_id = model.get_value( entry_iter, 7 )
            #         session_id = model.get_value( project_iter_parent, 7 )
            #         del self.session[ session_id ][ project_session_id ]
            #         model.remove( entry_iter )

            #         # если вдруг у проекта закончались дети, а юзер его не выделил
            #         if model.iter_children( project_iter_parent ):
            #             del self.session[ session_id ]
            #             model.remove( project_iter_parent )

            #     # удаляются все дети проекта перед удалением оного
            #     else:
            #         session_id = model.get_value( entry_iter, 7 )
            #         while project_entry_first_child:
            #             # на случай если будут проблемы с памятью ;-)
            #             # project_session_id = model.get_value( project_entry_first_child, 7 )
            #             # del self.session[ session_id ][ project_session_id ]
            #             model.remove( project_entry_first_child )
            #             project_entry_first_child = model.iter_children( entry_iter )
            #         del self.session[ session_id ]
            #         model.remove( entry_iter )

            self.refresh_add_tree_view()
    # debug
    def get_selected_paths(self, button):
        # model, selected_entries_path_list = self.addTreeView.get_selection().get_selected_rows()
        # for path in selected_entries_path_list:
        #     entry_iter = model.get_iter( path )
        #     print path
        #     print 'file: %s' % model.get_value( entry_iter, 1 )
        for k,i in self.session.iteritems():
            print 'session_id: %d, name: %s' % (k, i.filename)
            if not i.is_entry():
                for l,j in i.iteritems():
                    print '>> project_session_id: %d, name: %s' % (l, j.filename)

gobject.type_register( BackupDialog )
gobject.signal_new( 'ready-to-exec', BackupDialog, gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, () )