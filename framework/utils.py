# -*- coding: utf-8 -*-
import gtk
import gobject
from gtk import gdk
from datetime import datetime
from time import sleep
import os, re, urllib2, json, webbrowser, pickle, threading
import xml.etree.ElementTree as ET
import subprocess, tempfile, hashlib, shutil, binascii
import copy

TAPE_MATCH = re.compile( r'[\w\\]*(\d+)' )
ITDT_EXIT_CODE = re.compile( r'Exit with code: (\d+)' )
ITDT_LIST = re.compile(r'Filemark (\d+)')


NIX_DISKS = re.compile(r'(sd[a-z]+)')
NIX_ITDT_SCAN = re.compile(r'(/dev/IBMtape[0-9]+)')

WIN_WMIC_DISKS = re.compile(r'(PHYSICALDRIVE[0-9]+)')
WIN_ITDT_SCAN = re.compile(r'(Tape[0-9]+)')
WIN_CMD_LIST = re.compile(r'\w+=(.*)')
WIN_WMIC_DISK_TO_PARTITION_TO_LETTER_LIST = re.compile(r'\r\r\n\r\r\nAntecedent=".*="(.*)""\r\r\nDependent=".*="(.*)""')

NUM = re.compile(r'[0-9]+')
APP_TOKEN = 'YRXIV6D9KUGON97IS6LB3ZQD8WH6CXUW'

STORAGE_TYPES = [ 'DD', 'CD', 'SD' ]
STORAGE_STATES = [ 'CF', 'SE', 'UN' ]
ENTRY_TYPES = [ 'VT', 'OT' ]
GTK_STOCK_ICONS = [ gtk.STOCK_HARDDISK, gtk.STOCK_MEDIA_RECORD, gtk.STOCK_NETWORK, gtk.STOCK_DIRECTORY, gtk.STOCK_FILE ]

class Waiter(gtk.Window):

    def __init__(self, builder, parent_window = None):
        super(gtk.Window, self).__init__()
        self.builder = builder
        if parent_window:
            self.set_transient_for( parent_window )
            self.set_position( gtk.WIN_POS_CENTER_ON_PARENT )
        self.set_decorated(False)
        self.set_modal(True)
        self.set_resizable(False)

        self.vbox = self.builder.get_object('waiter_box')
        self.spinner = self.builder.get_object('waiter_spinner')
        self.info = self.builder.get_object('default_info')

        self.add( self.vbox )
        #self.set_sensitive(False)

    def set_parent( self, parent_window):
        self.set_transient_for( parent_window )
        self.set_position( gtk.WIN_POS_CENTER_ON_PARENT )

    def start_waiter(self):
        self.show_all()
        self.spinner.start()
        return False

    def stop_waiter(self):
        self.hide_all()
        self.spinner.stop()
        return False

class OperationManager():

    def __init__( self, os_platform ):
        self.located_devices = dict()
        self.os_platform = os_platform

    def reset_located_devices(self):
        self.located_devices.clear()

    def get_disks(self):

        self.reset_located_devices()

        if self.os_platform == 'posix':

            nix_disk = subprocess.Popen( 'ls -l /dev/disk/by-id | grep ata- | grep -v part', shell=True, stdout=subprocess.PIPE )
            nix_disk_out, nix_disk_err = nix_disk.communicate()
            matched_physical_disks = NIX_DISKS.findall( nix_disk_out )

            for num in xrange( len( matched_physical_disks ) ):
                posix_dev_name = os.path.join( '/dev/', matched_physical_disks[ num ] )
                posix_disk = {
                    'num' : num,
                    'model' : posix_dev_name,
                    'SN' : '11111'
                }
                self.located_devices[ posix_disk['num'] ] = posix_disk
        else:

            drives_list = subprocess.Popen( 'wmic diskdrive get name', shell=True, stdout=subprocess.PIPE )
            stdout_ret, stderr_ret = drives_list.communicate()
            drives = WIN_WMIC_DISKS.findall( stdout_ret )

            for drive in drives:
                disk_meta = dict()
                disk_meta['letters'] = []
                disk_meta['partitions'] = []
                disk_meta['name'] = drive
                disk_meta['num'] = int( NUM.search( drive ).group(0).strip(' \r\n\t') )
                
                drive_info = subprocess.Popen( r'wmic diskdrive where DeviceID="\\\\.\\{0}" get caption,model,size /format:list'.format(drive), shell=True, stdout=subprocess.PIPE )
                stdout_ret, stderr_ret = drive_info.communicate()
                drive_info_list = WIN_CMD_LIST.findall( stdout_ret )
                disk_meta['caption'] = drive_info_list[0].strip(' \r\n\t')
                disk_meta['model'] = drive_info_list[1].strip(' \r\n\t')
                disk_meta['size'] = int( drive_info_list[2] )
                
                drive_sn_raw = subprocess.Popen( r'wmic path win32_physicalmedia where tag="\\\\.\\{0}" get serialnumber /format:list'.format(drive), shell=True, stdout=subprocess.PIPE )
                stdout_ret, stderr_ret = drive_sn_raw.communicate()
                drive_sn = WIN_CMD_LIST.findall( stdout_ret )[0].strip(' \n\r\t')
                try:
                    int( drive_sn, 16 )
                    unhexed_str = binascii.unhexlify( drive_sn )
                    disk_sn = ''.join([ unhexed_str[x:x+2][::-1] for x in range(0, len(unhexed_str), 2) ])
                    disk_meta['SN'] = disk_sn.strip(r' \n\r\t')
                except ValueError:
                    disk_meta['SN'] = drive_sn.strip(' \r\n\t')
                    
                self.located_devices[ disk_meta['num'] ] = disk_meta
                
            disk_partition_raw = subprocess.Popen( 'WMIC path Win32_DiskDriveToDiskPartition get Antecedent,Dependent /format:list', shell=True, stdout=subprocess.PIPE )
            stdout_ret, stderr_ret = disk_partition_raw.communicate()
            disk_partition_list = WIN_WMIC_DISK_TO_PARTITION_TO_LETTER_LIST.findall( stdout_ret )


            partition_letter_raw = subprocess.Popen( 'WMIC path Win32_LogicalDiskToPartition get Antecedent,Dependent /format:list', shell=True, stdout=subprocess.PIPE )
            stdout_ret, stderr_ret = partition_letter_raw.communicate()
            partition_letter_list = WIN_WMIC_DISK_TO_PARTITION_TO_LETTER_LIST.findall( stdout_ret )



            for partition in disk_partition_list:

                disk_num = int( NUM.search(partition[0]).group(0).strip(' \r\n\t') )
                try:
                    self.located_devices[ disk_num ][ 'partitions' ].append( partition[1] )
                except KeyError: continue

            for (num,disk) in self.located_devices.iteritems():

                for partition_letter in partition_letter_list:

                    if partition_letter[0] in disk['partitions']:
                        disk['letters'].append( partition_letter[1] )

            for (num,disk) in self.located_devices.iteritems():

                disk_located_freespace = 0
                all_volumes_size = 0

                for letter in disk['letters']:
                    letter_freespace_raw = subprocess.Popen( r'wmic logicaldisk where DeviceID="{0}" get freespace,size /format:list'.format(letter), shell=True, stdout=subprocess.PIPE )
                    stdout_ret, stderr_ret = letter_freespace_raw.communicate()
                    letter_meta_list = WIN_CMD_LIST.findall( stdout_ret )
                    disk_located_freespace += int( letter_meta_list[0] )
                    all_volumes_size += int( letter_meta_list[1] )

                disk['freespace'] = int( disk['size'] ) - all_volumes_size + disk_located_freespace


    def get_streamers(self):

        self.reset_located_devices()

        path_to_itdt = os.path.join( os.getcwdu(), 'ibm' )

        if self.os_platform == 'posix':
            itdt_exec = os.path.join( path_to_itdt, 'itdt.sh' )
            itdt_lto_list = subprocess.Popen( [ itdt_exec, 'scan' ], stdout=subprocess.PIPE )
            itdt_out, itdt_err = itdt_lto_list.communicate()
            matched_lto = NIX_ITDT_SCAN.findall( itdt_out )

            if not matched_lto:
                print 'info: have no connected LTO devices!'
                return

            # for name in matched_lto:
            #     gobject.idle_add( self.device_combo_append, name )                
        else:
            itdt_exec = os.path.join( path_to_itdt, 'itdt.exe' )
            itdt_lto_list = subprocess.Popen( [ itdt_exec, 'scan' ], stdout=subprocess.PIPE )
            itdt_out, itdt_err = itdt_lto_list.communicate()
            matched_lto = WIN_ITDT_SCAN.findall( itdt_out )

            if not matched_lto:
                print 'info: have no connected LTO devices!'
                return
